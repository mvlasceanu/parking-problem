<?php

use App\Barrier;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barriers', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('direction', [Barrier::ENTRY, Barrier::EXIT]);
            $table->enum('state', [
                                            Barrier::STATE_AUTO,
                                            Barrier::STATE_MANUAL,
                                            Barrier::STATE_OPEN,
                                            Barrier::STATE_CLOSED,
                                            Barrier::STATE_LOCKED])->default(Barrier::STATE_AUTO);
            $table->unsignedInteger('parking_lot_id');
            $table->timestamps();
            $table->index(['direction']);
            $table->index(['parking_lot_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barriers');
    }
}
