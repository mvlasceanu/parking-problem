<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('is_parked')->default(false);
            $table->timestamp('entry_at', 0)->nullable()->default(null);
            $table->timestamp('exit_at', 0)->nullable()->default(null);
            $table->unsignedInteger('parking_lot_id')->nullable()->default(null);
            $table->timestamps();
            $table->index(['parking_lot_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
