<?php

namespace Tests\Unit;

use App\Barrier;
use App\ParkingLot;
use App\ParkingLotConfig;
use App\ParkingManager;
use App\Vehicle;
use App\VehicleManager;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;

class ParkingLotManagerTest extends TestCase
{
    /**
     * @var ParkingManager instance
     */
    private $_plm;

    /**
     * @var ParkingLotConfig config
     */
    private $_pl_config;

    /**
     * @var ParkingLot parling lot
     */
    private $_pl;

    /**
     * @var VehicleManager
     */
    private $_vm;

    /**
     * Our UT setup
     */
    public function setUp()
    {
        parent::setUp();

        // ParkingManager singleton
        $this->_plm = ParkingManager::getInstance();

        // Config for the parking lot
        $this->_pl_config = new ParkingLotConfig();
        $this->_pl_config->setEntryBarriersCount(2);
        $this->_pl_config->setExitBarriersCount(2);
        $this->_pl_config->setParkingSpots(2);
        $this->_pl_config->setPollutionBuffer(100.0);

        // Vehicle Manager
        $this->_vm = VehicleManager::getInstance();

    }

    /**
     * Test create Barriers
     * @throws \Exception
     */
    public function testCreateBarriersTest()
    {
        // Parking lot object to work with
        $parkingLot = ParkingLot::create([
            'max_pollution_level' => $this->_pl_config->getPollutionBuffer()
        ]);
        $barriers = $this->_plm->createBarriers($parkingLot, $this->_pl_config);
        $this->assertEquals(Collection::class, get_class($barriers));
        $this->assertEquals(4, $barriers->count());
        $this->assertEquals(2, $parkingLot->barriers(Barrier::ENTRY)->get()->count());
        $this->assertEquals(2, $parkingLot->barriers(Barrier::EXIT)->get()->count());

        $this->removeParkingLot($parkingLot);
    }

    /**
     * Test if the available spots count returns what it should
     * @throws \Exception
     */
    public function testAvailableSpotsCount()
    {
        // Create parking lot
        $parkingLot = $this->_plm->createParkingLot($this->_pl_config);

        // Total available spots. Should be 0
        $count = $this->_plm->availableSpotsCount($parkingLot);
        $this->assertEquals(2, $count);

        // Park a vehicle
        $vehicle = Vehicle::create([
            'is_parked' => false
        ]);

        $barrier = $parkingLot->barriers(Barrier::ENTRY)->get()->first();
        $this->_vm->enterParkingLot($vehicle, $barrier);

        // Make sure that there are still 2 spots available, vehicle didnt park yet
        $this->assertEquals(2, $this->_plm->availableSpotsCount($parkingLot));

        // Park the vehicle. There should be 1 parking spots left
        $this->_vm->park($vehicle, $parkingLot, true);
        $this->assertEquals(1, $this->_plm->availableSpotsCount($parkingLot));

        // Unpark the vehicle. There should be 2 spots now
        $this->_vm->park($vehicle, $parkingLot, false);
        $this->assertEquals(2, $this->_plm->availableSpotsCount($parkingLot));

        // Remove the parking lot
        $this->removeParkingLot($parkingLot);
    }

    /**
     * @throws \Exception
     */
    public function testRunningCarsCount()
    {
        // Create parking lot
        $parkingLot = $this->_plm->createParkingLot($this->_pl_config);

        // No Cars
        $this->assertEquals(0, $this->_plm->runningCarsCount($parkingLot));

        // Make some
        $this->addTwoCars($parkingLot);

        // We do have one unparked car
        $this->assertEquals(2, $this->_plm->runningCarsCount($parkingLot));

        // Park all the vehicles
        $parkingLot->vehicles->each(function (Vehicle $item, $key) use($parkingLot) {
            VehicleManager::getInstance()->park($item, $parkingLot);
        });

        $this->assertEquals(0, $this->_plm->runningCarsCount($parkingLot));

        // Remove the parking lot
        $this->removeParkingLot($parkingLot);
    }

    /**
     * @throws \Exception
     */
    public function testPollutionBuffer()
    {
        // Create parking lot
        $parkingLot = $this->_plm->createParkingLot($this->_pl_config);

        // Should be 0 pollution buffer with no cars
        $this->assertEquals(0, $this->_plm->pollutionBuffer($parkingLot));

        // Make some cars
        $this->addTwoCars($parkingLot);

        // Should be 100 pollution buffer with 2 cars running
        $this->assertEquals(100, $this->_plm->pollutionBuffer($parkingLot));

        // Park one vehicle
        $randomVehicle = $parkingLot->vehicles->random(1)->first();
        $this->_vm->park($randomVehicle, $parkingLot, true);

        // Should be 50 pollution buffer with 1 car running
        $this->assertEquals(50, $this->_plm->pollutionBuffer($parkingLot));

        $this->removeParkingLot($parkingLot);
    }

    /**
     * @throws \Exception
     */
    public function testIsParkingFull()
    {
        // Create parking lot
        $parkingLot = $this->_plm->createParkingLot($this->_pl_config);

        // Should be false, no cars are in
        $this->assertEquals(false, $this->_plm->isParkingFull($parkingLot));

        // Add cars
        $this->addTwoCars($parkingLot);

        // Should be true, there are 2 cars inside
        $this->assertEquals(true, $this->_plm->isParkingFull($parkingLot));

        // One exits
        $exit = $parkingLot->barriers(Barrier::EXIT)->get()->first();
        $randomVehicle = $parkingLot->vehicles->random(1)->first();
        $this->_vm->exitParkingLot($randomVehicle, $exit);

        // Should be false, there are 1 cars inside
        $this->assertEquals(false, $this->_plm->isParkingFull($parkingLot));

        $this->removeParkingLot($parkingLot);
    }

    private function addTwoCars(ParkingLot &$parkingLot)
    {
        $barrier = $parkingLot->barriers(Barrier::ENTRY)->get()->first();
        for($i = 0; $i < 2; $i++) {
            $vehicle = Vehicle::create([
                'is_parked' => false
            ]);

            $this->_vm->enterParkingLot($vehicle, $barrier);
        }

        $parkingLot = $parkingLot->fresh();

        return $parkingLot;
    }

    /**
     * @throws \Exception
     */
    public function isPollutionLevelReached()
    {
        // Create parking lot
        $parkingLot = $this->_plm->createParkingLot($this->_pl_config);

        // Should be false, no cars are in
        $this->assertEquals(false, $this->_plm->isPollutionLevelReached($parkingLot));

        // Add 2 cars
        $this->addTwoCars($parkingLot);

        // Should be true, 2 cars are in, not parked yet
        $this->assertEquals(true, $this->_plm->isPollutionLevelReached($parkingLot));

        // Park all the vehicles
        $parkingLot->vehicles->each(function (Vehicle $item, $key) use($parkingLot) {
            VehicleManager::getInstance()->park($item, $parkingLot);
        });

        // Should be false, 2 cars are in, parked
        $this->assertEquals(false, $this->_plm->isPollutionLevelReached($parkingLot));

        // Unpark one car
        $this->_vm->park($parkingLot->vehicles->first(), $parkingLot, false);

        // Should be false, only 1 car parked
        $this->assertEquals(false, $this->_plm->isPollutionLevelReached($parkingLot));

        $this->removeParkingLot($parkingLot);
    }

    /**
     * @throws \Exception
     */
    public function canEnter()
    {
        // Create parking lot
        $parkingLot = $this->_plm->createParkingLot($this->_pl_config);

        $entry_barrier = $parkingLot->barriers(Barrier::ENTRY)->get()->first();
        $exit_barrier = $parkingLot->barriers(Barrier::EXIT)->get()->first();

        // Should be true, no cars, unlocked entry barrier
        $this->assertTrue($this->_plm->canEnter($entry_barrier));

        // Should be false, exit barrier
        $this->assertFalse($this->_plm->canEnter($exit_barrier));

        $entry_barrier->locked(true);
        // Should be false, locked barrier
        $this->assertFalse($this->_plm->canEnter($exit_barrier));

        // Add 2 cars
        $this->addTwoCars($parkingLot);

        // Should be false, full parking lot
        $this->assertFalse($this->_plm->canEnter($exit_barrier));

        $this->removeParkingLot($parkingLot);
    }

    /**
     * @throws \Exception
     */
    public function testCanExit()
    {
        // Create parking lot
        $parkingLot = $this->_plm->createParkingLot($this->_pl_config);

        $entry_barrier = $parkingLot->barriers(Barrier::ENTRY)->get()->first();
        $exit_barrier = $parkingLot->barriers(Barrier::EXIT)->get()->first();

        // Should be true, no cars, unlocked exit barrier
        $this->assertTrue($this->_plm->canExit($exit_barrier));

        // Should be false, entry barrier
        $this->assertFalse($this->_plm->canExit($entry_barrier));

        $this->removeParkingLot($parkingLot);
    }

    /**
     * @throws \Exception
     */
    public function testVehicleIsInside()
    {
        // Create parking lot
        $parkingLot = $this->_plm->createParkingLot($this->_pl_config);

        // Park a vehicle
        $vehicle = Vehicle::create([
            'is_parked' => false
        ]);

        // Did not enter yet
        $this->assertFalse($this->_plm->vehicleIsInside($vehicle, $parkingLot));

        $entry_barrier = $parkingLot->barriers(Barrier::ENTRY)->get()->first();
        $this->_vm->enterParkingLot($vehicle, $entry_barrier);

        $parkingLot = $parkingLot->fresh();

        // Inside
        $this->assertTrue($this->_plm->vehicleIsInside($vehicle, $parkingLot));

        // Park it, should still be inside
        $this->_vm->park($vehicle, $parkingLot, true);
        $parkingLot = $parkingLot->fresh();
        $this->assertTrue($this->_plm->vehicleIsInside($vehicle, $parkingLot));

        // Exit
        $exit_barrier = $parkingLot->barriers(Barrier::EXIT)->get()->first();
        $this->_vm->exitParkingLot($vehicle, $exit_barrier);
        $parkingLot = $parkingLot->fresh();
        $this->assertFalse($this->_plm->vehicleIsInside($vehicle, $parkingLot));

        $this->removeParkingLot($parkingLot);
    }

    /**
     * @throws \Exception
     */
    public function testCreateParkingSpots()
    {
        $parkingLot = ParkingLot::create([
            'max_pollution_level' => $this->_pl_config->getPollutionBuffer()
        ]);
        $spots = $this->_plm->createParkingSpots($parkingLot, $this->_pl_config->getParkingSpots());
        $this->assertEquals(Collection::class, get_class($spots));
        $this->assertEquals(2, $spots->count());

        $this->removeParkingLot($parkingLot);
    }

    /**
     * @param ParkingLot $parkingLot
     * @throws \Exception
     */
    private function removeParkingLot(ParkingLot $parkingLot)
    {
        $parkingLot->vehicles()->forceDelete();
        $parkingLot->parkingSpots()->forceDelete();
        $parkingLot->barriers()->forceDelete();
        $parkingLot->forceDelete();
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}
