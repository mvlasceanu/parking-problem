<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * @property mixed id
 * @property Collection parkingSpots
 * @property Collection vehicles
 * @property float max_pollution_level
 * @property Collection barriers
 */
class ParkingLot extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['max_pollution_level'];

    /**
     * Vehicles in the parking lot
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    }

    /**
     * Parking lot's barriers
     *
     * @param bool $direction
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function barriers($direction = false)
    {
        $barriers = $this->hasMany(Barrier::class);
        switch ($direction) {
            case Barrier::ENTRY:
                return $barriers->where('direction', Barrier::ENTRY);
                break;

            case Barrier::EXIT:
                return $barriers->where('direction', Barrier::EXIT);
                break;

            default:
                return $barriers;
        }
    }

    /**
     * Parking spots within this parking lot
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parkingSpots()
    {
        return $this->hasMany(ParkingSpot::class);
    }
}
