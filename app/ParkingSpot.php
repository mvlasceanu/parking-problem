<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property boolean is_occupied
 */
class ParkingSpot extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['parking_lot_id', 'is_occupied'];

    /**
     * The parking lot this spot belongs to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parkingLot()
    {
        return $this->belongsTo(ParkingLot::class);
    }

    public function isOccupied()
    {
        return $this->is_occupied;
    }
}
