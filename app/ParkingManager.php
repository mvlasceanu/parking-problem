<?php
/**
 * Created by PhpStorm.
 * User: Mihai
 * Date: 5/10/2018
 * Time: 2:51 PM
 */

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Collection;

class ParkingManager
{
    const REASON_B_LOCKED = 'Sorry, the barrier is locked.';
    const REASON_B_WRONG_IN = 'Sorry, you cannot use this barrier to get in.';
    const REASON_B_WRONG_OUT = 'Sorry, you cannot use this barrier to get out.';
    const REASON_PB_REACHED = 'Sorry, pollution buffer reached.';
    const REASON_P_FULL = 'Sorry, parking is full';
    const REASON_UNKNOWN = 'Unknown';

    /**
     * Class singleton
     *
     * @var $_instance
     */
    private static $_instance = null;

    /**
     * Prevent external instantiation and init some props
     *
     * ParkingManager constructor.
     */
    protected function __construct()
    {
    }

    /**
     * @param ParkingLotConfig $config
     * @return  ParkingLot
     * @throws \Exception
     */
    public function createParkingLot(ParkingLotConfig $config)
    {
        // Create the object
        $parkingLot = ParkingLot::create([
            'max_pollution_level' => $config->getPollutionBuffer()
        ]);

        // Create Barriers
        $this->createBarriers($parkingLot, $config);

        // Parking Spots
        $this->createParkingSpots($parkingLot, $config->getParkingSpots());

        return $parkingLot;
    }

    /**
     * Handles the creation of the Barrier objects of a parking lot
     *
     * @param ParkingLot $parkingLot
     * @param ParkingLotConfig $config
     * @return \Illuminate\Support\Collection
     */
    public function createBarriers(ParkingLot $parkingLot, ParkingLotConfig $config)
    {
        // Entry barriers
        for ($i = 0; $i < $config->getEntryBarriersCount(); $i++) {
            Barrier::create([
                'direction' => Barrier::ENTRY,
                'parking_lot_id' => $parkingLot->id
            ]);
        }

        // Exit barriers
        for ($j = 0; $j < $config->getExitBarriersCount(); $j++) {
            Barrier::create([
                'direction' => Barrier::EXIT,
                'parking_lot_id' => $parkingLot->id
            ]);
        }

        return $parkingLot->barriers;
    }

    /**
     * Init all the parking spots
     *
     * @param ParkingLot $parkingLot
     * @param int $spots
     * @return Collection
     * @throws \Exception
     */
    public function createParkingSpots(ParkingLot $parkingLot, int $spots)
    {
        if ($parkingLot->id <= 0) {
            throw new \Exception('Sorry, parking lot ID is missing');
        }

        for ($i = 0; $i < $spots; $i++) {
            $spot = ParkingSpot::create([
                'parking_lot_id' => $parkingLot->id,
                'is_occupied' => false
            ]);
        }

        return $parkingLot->parkingSpots;
    }

    /**
     * Counts available spots in the parking lot
     *
     * @param ParkingLot $parkingLot
     * @return int
     */
    public function availableSpotsCount(ParkingLot $parkingLot)
    {
        $free = $parkingLot->parkingSpots->reject(function (ParkingSpot $spot) {
            return $spot->is_occupied;
        });

        return $free->count();
    }

    /**
     * Count of running cars (engine on)
     * @param ParkingLot $parkingLot
     * @return int
     */
    public function runningCarsCount(ParkingLot $parkingLot)
    {
        $running = $parkingLot->vehicles->reject(function (Vehicle $vehicle) {
            return $vehicle->is_parked || !is_null($vehicle->exit_at);
        });

        return $running->count();
    }

    /**
     * Occupied spots count
     *
     * @param ParkingLot $parkingLot
     * @return bool|int
     */
    public function occupiedSpots(ParkingLot $parkingLot)
    {
        if ($parkingLot->vehicles->count() === 0) {
            return false;
        }

        return $parkingLot->vehicles->reject(function (Vehicle $vehicle) {
            return !$vehicle->is_parked && is_null($vehicle->exit_at);
        })->count();
    }

    /**
     * Current pollution buffer
     *
     * @param ParkingLot $parkingLot
     * @return float|int
     */
    public function pollutionBuffer(ParkingLot $parkingLot)
    {
        $parkingLot = $parkingLot->fresh();
        if ($parkingLot->vehicles->count() === 0 || $this->runningCarsCount($parkingLot) === 0) {
            return 0;
        }

        return 100 * $this->runningCarsCount($parkingLot) / $parkingLot->parkingSpots->count();
    }

    /**
     * Are all spaces occupied
     *
     * @param ParkingLot $parkingLot
     * @return bool
     */
    public function isParkingFull(ParkingLot $parkingLot)
    {
        $collection = $parkingLot->vehicles->where('entry_at', '!=', NULL)
            ->where('exit_at', '=', NULL);
        return $collection->count() === $parkingLot->parkingSpots->count();
    }

    /**
     * Checks if the current pollution limits are exceeded
     *
     * @param ParkingLot $parkingLot
     * @return bool
     */
    public function isPollutionLevelReached(ParkingLot $parkingLot)
    {
        return (float)$this->pollutionBuffer($parkingLot) > (float)$parkingLot->max_pollution_level;
    }

    /**
     * Checks if a vehicle is allowed in the parking lot
     *
     * @param Barrier $barrier
     * @return bool
     */
    public function canEnter(Barrier $barrier)
    {
        if (in_array($barrier->state, [Barrier::STATE_LOCKED])) {
            return false;
        }

        // No entry on an exit barrier
        if ($barrier->direction !== Barrier::ENTRY) {
            return false;
        }

        // No entry if parking is full
        if ($this->isParkingFull($barrier->parkingLot)) {
            return false;
        }

        // No entry if pollution level is reached
        if ($this->isPollutionLevelReached($barrier->parkingLot)) {
            return false;
        }

        return true;
    }

    /**
     * Messages for no entry reasons
     *
     * @param ParkingLot $parkingLot
     * @param Barrier $barrier
     * @return bool|string
     */
    public function getNoEntryReason(ParkingLot $parkingLot, Barrier $barrier)
    {
        if (in_array($barrier->state, [Barrier::STATE_LOCKED])) {
            return self::REASON_B_LOCKED;
        }

        // No entry on an exit barrier
        if ($barrier->direction !== Barrier::ENTRY) {
            return self::REASON_B_WRONG_IN;
        }

        // No entry if parking is full
        if ($this->isParkingFull($barrier->parkingLot)) {
            return self::REASON_P_FULL;
        }

        // No entry if pollution level is reached
        if ($this->isPollutionLevelReached($barrier->parkingLot)) {
            return self::REASON_PB_REACHED;
        }

        return self::REASON_UNKNOWN;
    }

    /**
     * Some messages for no exit
     *
     * @param ParkingLot $parkingLot
     * @param Barrier $barrier
     * @return string
     */
    public function getNoExitReason(ParkingLot $parkingLot, Barrier $barrier)
    {
        if (in_array($barrier->state, [Barrier::STATE_LOCKED])) {
            return self::REASON_B_LOCKED;
        }

        // No exit on an entry barrier
        if ($barrier->direction !== Barrier::EXIT) {
            return self::REASON_B_WRONG_OUT;
        }

        return self::REASON_UNKNOWN;
    }

    /**
     * Can the vehicle exit through the barrier?
     *
     * @param Barrier $barrier
     * @return bool
     */
    public function canExit(Barrier $barrier)
    {
        if (in_array($barrier->state, [Barrier::STATE_LOCKED])) {
            return false;
        }

        if ($barrier->direction !== Barrier::EXIT) {
            return false;
        }

        return true;
    }

    /**
     * Allow vehicle inside the parking lot
     *
     * @param Vehicle $vehicle
     * @param Barrier $barrier
     * @return mixed
     */
    public function allowVehicle(Vehicle $vehicle, Barrier $barrier)
    {
        $exists = $barrier->parkingLot->vehicles->where('id', $vehicle->id)->last();

        if (!$exists) {
            $vehicle->parking_lot_id = $barrier->parkingLot->id;
            $vehicle->save();

            return $vehicle;
        }

        return $exists;
    }

    /**
     * Singleton instance of this class
     *
     * @return ParkingManager|null
     */
    public static function getInstance()
    {
        if (is_null(self::$_instance) || !(self::$_instance instanceof ParkingManager)) {
            self::$_instance = new ParkingManager();
        }

        return self::$_instance;
    }

    /**
     * Basic check if the vehicle is inside the parking lot already
     *
     * @param Vehicle $vehicle
     * @param ParkingLot $parkingLot
     * @return bool
     */
    public function vehicleIsInside(Vehicle $vehicle, ParkingLot $parkingLot)
    {
        if ($parkingLot->vehicles->count() === 0) {
            return false;
        }

        $vehicle = $parkingLot->vehicles->filter(function (Vehicle $v) use ($vehicle) {
            return $v->id == $vehicle->id;
        })->sortBy('entry_at')->last();

        if (is_a($vehicle, Vehicle::class)) {
            return is_null($vehicle->exit_at) && !is_null($vehicle->entry_at);
        }

        return false;
    }

    /**
     * Picks random spot to park
     *
     * @param ParkingLot $parkingLot
     * @param bool $free
     * @return ParkingLot|bool
     */
    public function pickParkingSpot(ParkingLot $parkingLot, $free = true)
    {
        $spots = $parkingLot->parkingSpots->filter(function (ParkingSpot $spot) use ($free) {
            return (bool)$spot->is_occupied === !$free;
        });

        if ($spots->count() > 0) {
            return $spots->random();
        }

        return false;
    }

    /**
     * Pick a car that is not parked, but inside
     *
     * @param ParkingLot $parkingLot
     * @return bool|mixed
     */
    public function pickRandomUnparkedVehicle(ParkingLot $parkingLot)
    {
        if ($parkingLot->vehicles->count() === 0) {
            return false;
        }

        $ttl = $parkingLot->vehicles->reject(function (Vehicle $vehicle) {
            return $vehicle->is_parked || !is_null($vehicle->exit_at);
        });

        if($ttl->count() > 0) {
            return $ttl->random(1)->first();
        }

        return false;
    }

    /**
     * Pick randoml parked vehicle
     *
     * @param ParkingLot $parkingLot
     * @return bool|mixed
     */
    public function pickRandomParkedVehicle(ParkingLot $parkingLot)
    {
        if ($parkingLot->vehicles->count() === 0) {
            return false;
        }

        $ttl = $parkingLot->vehicles->reject(function (Vehicle $vehicle) {
            return !$vehicle->is_parked && is_null($vehicle->exit_at);
        });

        if($ttl->count() > 0) {
            return $ttl->random(1)->first();
        }

        return false;
    }

    /**
     * Disallow cloning using 'clone'
     */
    private function __clone()
    {
    }

    /**
     * Disallow cloning using unserialize()
     */
    private function __wakeup()
    {
    }
}