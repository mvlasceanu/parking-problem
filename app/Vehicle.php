<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property bool is_parked
 * @property Carbon entry_at
 * @property Carbon exit_at
 * @property mixed id
 * @property int parking_lot_id
 */
class Vehicle extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['is_parked', 'entry_at', 'exit_at', 'parking_lot_id'];

    /**
     * Mark vehicle as parked
     * @param bool $parked
     */
    public function parked(bool $parked)
    {
        $this->is_parked = $parked;
        $this->save();
    }

    /**
     * Vehicle exists parking lot
     */
    public function exitParkingLot()
    {
        $this->exit_at = Carbon::now();
        $this->save();
    }

    /**
     * Vehicle enters inside the parking lot, not parked yet
     */
    public function enterParkingLot()
    {
        $this->entry_at = Carbon::now();
        $this->save();
    }
}
