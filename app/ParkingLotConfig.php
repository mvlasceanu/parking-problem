<?php
/**
 * Created by PhpStorm.
 * User: Mihai
 * Date: 5/12/2018
 * Time: 3:41 PM
 */

namespace App;


class ParkingLotConfig
{
    /**
     * Number of parking spots
     *
     * @var int $_parkingSpots
     */
    private $_parkingSpots = 0;

    /**
     * Number of entry barriers
     *
     * @var int $_entryBarriersCount
     */
    private $_entryBarriersCount = 0;

    /**
     * Number of exit barriers
     *
     * @var int $_exitBarriersCount
     */
    private $_exitBarriersCount = 0;

    /**
     * Max Pollution buffer (in %)
     *
     * @var float $_pollutionBuffer
     */
    private $_pollutionBuffer = 0.00;

    /**
     * @return int
     */
    public function getParkingSpots(): int
    {
        return $this->_parkingSpots;
    }

    /**
     * @param int $parkingSpots
     */
    public function setParkingSpots(int $parkingSpots): void
    {
        $this->_parkingSpots = $parkingSpots;
    }

    /**
     * @return int
     */
    public function getEntryBarriersCount(): int
    {
        return $this->_entryBarriersCount;
    }

    /**
     * @param int $entryBarriersCount
     */
    public function setEntryBarriersCount(int $entryBarriersCount): void
    {
        $this->_entryBarriersCount = $entryBarriersCount;
    }

    /**
     * @return int
     */
    public function getExitBarriersCount(): int
    {
        return $this->_exitBarriersCount;
    }

    /**
     * @param int $exitBarriersCount
     */
    public function setExitBarriersCount(int $exitBarriersCount): void
    {
        $this->_exitBarriersCount = $exitBarriersCount;
    }

    /**
     * @return float
     */
    public function getPollutionBuffer(): float
    {
        return $this->_pollutionBuffer;
    }

    /**
     * @param float $pollutionBuffer
     */
    public function setPollutionBuffer(float $pollutionBuffer): void
    {
        $this->_pollutionBuffer = $pollutionBuffer;
    }
}