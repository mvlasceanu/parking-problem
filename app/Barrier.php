<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string direction
 * @property ParkingLot parkingLot
 * @property string state
 */
class Barrier extends Model
{
    /**
     * Entry barrier
     */
    const ENTRY = 'entry';

    /**
     * Exit Barrier
     */
    const EXIT = 'exit';

    /**
     * Possible barrier states
     */
    const STATE_MANUAL = 'manual';
    const STATE_AUTO = 'auto';
    const STATE_OPEN = 'open';
    const STATE_CLOSED = 'closed';
    const STATE_LOCKED = 'locked';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['direction', 'state', 'parking_lot_id'];

    /**
     * Parking lot this barrier belongs to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parkingLot()
    {
        return $this->belongsTo(ParkingLot::class);
    }

    /**
     * Locks or unlocks the barrier
     *
     * @param bool $locked
     */
    public function locked($locked = true)
    {
        $this->is_locked = $locked;
        $this->save();
    }

    public function open($open = false)
    {
        $this->is_open = $open;
        $this->save();
    }
}
