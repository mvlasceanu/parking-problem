<?php

namespace App\Console\Commands;

use App\Barrier;
use App\ParkingLot;
use App\ParkingLotConfig;
use App\ParkingManager;
use App\VehicleManager;
use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Storage;

class RunInput extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runinput:parking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var ParkingManager $manager
     */
    private $manager;

    /**
     * @var VehicleManager $v_manager
     */
    private $v_manager;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->manager = ParkingManager::getInstance();
        $this->v_manager = VehicleManager::getInstance();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $input = $this->parseInput();
        $output = $this->parseOutput();
        $parkingLot = $this->setupParkingLot($input);

        if(is_a($parkingLot, ParkingLot::class)) {
            $this->handleEntries($input, $parkingLot);
            $this->handleParking($input, $parkingLot);
            $this->handleExists($input, $parkingLot);
        } else {
            $this->error("Sorry, an error occurred while trying to create the parking lot.");
        }
    }

    /**
     * Stats Messages
     * @param ParkingLot $parkingLot
     */
    public function stats(ParkingLot $parkingLot)
    {
        $this->info(sprintf('Parked cars: %d', $this->manager->occupiedSpots($parkingLot)));
        $this->info(sprintf('Running cars: %d', $this->manager->runningCarsCount($parkingLot)));
        $this->info(sprintf('Total spots: %d', $parkingLot->parkingSpots->count()));
        $this->info(sprintf('Total cars: %d', $parkingLot->vehicles->count()));
    }

    /**
     * Creates the parking lot
     *
     * @param array $input
     * @return ParkingLot|bool
     */
    private function setupParkingLot($input = [])
    {
        $config = new ParkingLotConfig();
        $config->setParkingSpots((int)$input['N']);
        $config->setPollutionBuffer(100);
        $config->setExitBarriersCount(1);
        $config->setEntryBarriersCount(2);

        try {
            return $this->manager->createParkingLot($config);
        } catch (\Exception $e) {
            $this->error(sprintf('Error in creating the parking lot', $e->getMessage()));
        }

        return false;
    }

    /**
     * @param $input
     * @param ParkingLot $parkingLot
     * @throws \Exception
     */
    public function handleExists($input, ParkingLot $parkingLot)
    {
        foreach ($input['E'] as $exits) {
            switch ($exits) {
                case 1:
                    if ($parkingLot->vehicles->count() > 0) {
                        $exit = $parkingLot->barriers(Barrier::EXIT)->get()->first();
                        $vehicle = $parkingLot->vehicles->random(1)->first();
                        $this->v_manager->exitParkingLot($vehicle, $exit);
                        $this->info(sprintf('Vehicle %d left the parking lot', $vehicle->id));
                    }
                    break;
                default;
            }
        }
    }

    /**
     * @param $input
     * @param ParkingLot $parkingLot
     * @throws \Exception
     */
    private function handleParking($input, ParkingLot $parkingLot)
    {
        foreach ($input['P'] as $park) {
            switch ($park) {
                case 1:
                    $vehicle = $this->manager->pickRandomUnparkedVehicle($parkingLot);
                    if ($vehicle) {
                        $this->v_manager->park($vehicle, $parkingLot, true);
                        $this->info(sprintf('Vehicle %d parked', $vehicle->id));
                    }

                    $this->stats($parkingLot);
                    break;
                case 2:
                    $vehicle = $this->manager->pickRandomParkedVehicle($parkingLot);
                    if ($vehicle) {
                        $this->v_manager->park($vehicle, $parkingLot, false);
                        $this->info(sprintf('Vehicle %d left the parking spot', $vehicle->id));
                    }
                    $this->stats($parkingLot);
                    break;
                default;
            }
        }
    }

    /**
     * Gate entries
     *
     * @param $input
     * @param ParkingLot $parkingLot
     */
    private function handleEntries($input, ParkingLot $parkingLot)
    {
        $b1 = $parkingLot->barriers(Barrier::ENTRY)->get()->first();
        $b2 = $parkingLot->barriers(Barrier::ENTRY)->get()->last();

        foreach ($input['I'] as $entry) {
            for ($i = 0; $i < count($entry); $i++) {
                $e1 = $entry[$i];
                $e2 = $entry[$i];
                switch ($e1) {
                    case 1:
                        $vehicle = $this->v_manager->makeVehicle();
                        if($this->manager->canEnter($b1)) {
                            $this->v_manager->enterParkingLot($vehicle, $b1);
                            $this->info(sprintf('Vehicle %d entered the parking lot via barrier 1', $vehicle->id));
                        } else {
                            $this->info($this->manager->getNoEntryReason($parkingLot, $b1));
                        }

                        $this->stats($parkingLot);
                        break;
                    default;
                }

                switch ($e2) {
                    case 1:
                        $vehicle = $this->v_manager->makeVehicle();
                        if($this->manager->canEnter($b2)) {
                            $this->v_manager->enterParkingLot($vehicle, $b2);
                            $this->info(sprintf('Vehicle %d entered the parking lot via barrier 1', $vehicle->id));
                        } else {
                            $this->info($this->manager->getNoEntryReason($parkingLot, $b2));
                        }

                        $this->stats($parkingLot);
                    default;
                }
            }
        }
    }

    /**
     * Parse input.txt
     *
     * @return array
     */
    private function parseInput()
    {
        try {
            $return = [];

            $input = Storage::disk('local')->get('input.txt');

            $data_input = explode("\n", $input);

            // Parking spots
            $spots = (int)str_replace('N:', '', $data_input[0]);
            $return['N'] = $spots;

            // Park entries
            $entries = (int)str_replace('X:', '', $data_input[1]);
            $return['X'] = $entries;

            // Entry Sequences
            $sequence_str1 = str_replace('I:', '', $data_input[2]);
            $sequence_str2 = str_replace('I:', '', $data_input[3]);
            $return['I'] = [];
            $return['I'][] = str_split($sequence_str1);
            $return['I'][] = str_split($sequence_str2);

            // Parking sequence
            $parking_sequence_str = str_replace('P:', '', $data_input[4]);
            $return['P'] = str_split($parking_sequence_str);

            // Exit sequence
            $exit_sequence_str = str_replace('E:', '', $data_input[5]);
            $return['E'] = str_split($exit_sequence_str);

            return $return;
        } catch (FileNotFoundException $e) {
            $this->error(sprintf('Could not parse the input file.', $e->getMessage()));
        }
    }

    /**
     * Parse output.txt
     */
    public function parseOutput()
    {
        try {
            $output = Storage::disk('local')->get('output.txt');

            $return = [];

            $data_output = explode("\n", $output);

            // Total cars sequence
            $sequence_ttl_cars_str = str_replace('T:', '', $data_output[0]);
            $return['T'] = str_split($sequence_ttl_cars_str);

            // Park entries
            $sequence_parked_cars_str = (int)str_replace('P:', '', $data_output[1]);
            $return['P'] = str_split($sequence_parked_cars_str);

            // Entry Sequences
            $sequence_running_cars_str = str_replace('R:', '', $data_output[2]);
            $return['R'] = str_split($sequence_running_cars_str);

        } catch (FileNotFoundException $e) {
            $this->error(sprintf('Could not parse the output file.', $e->getMessage()));
        }
    }


}
