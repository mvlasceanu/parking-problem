<?php
/**
 * Created by PhpStorm.
 * User: Mihai
 * Date: 5/12/2018
 * Time: 1:53 PM
 */

namespace App;


class VehicleManager
{
    /**
     * Class singleton
     *
     * @var VehicleManager $_instance
     */
    private static $_instance = null;

    /**
     * Prevent external instantiation and init some props
     *
     * VehicleManager constructor.
     */
    protected function __construct() {}

    /**
     * Singleton instance of this class
     *
     * @return VehicleManager|null
     */
    public static function getInstance()
    {
        if (is_null(self::$_instance) || !(self::$_instance instanceof VehicleManager)) {
            self::$_instance = new VehicleManager();
        }

        return self::$_instance;
    }

    /**
     * Vehicle tries to enter the parking lot
     *
     * @param Vehicle $vehicle
     * @param Barrier $barrier
     * @return bool
     */
    public function enterParkingLot(Vehicle $vehicle, Barrier $barrier)
    {
        $parking_manager = ParkingManager::getInstance();

        if($parking_manager->canEnter($barrier)) {
            $plv = $parking_manager->allowVehicle($vehicle, $barrier);
            if(!is_null($plv)) {
                $vehicle->enterParkingLot();
                return true;
            }
        }

        return false;
    }

    /**
     * Vehicle exits the parking lot
     *
     * @param Vehicle $vehicle
     * @param Barrier $barrier
     * @return bool
     * @throws \Exception
     */
    public function exitParkingLot(Vehicle $vehicle, Barrier $barrier)
    {
        $parking_manager = ParkingManager::getInstance();
        if($parking_manager->canExit($barrier)) {
            $this->park($vehicle, $barrier->parkingLot, false);
            $vehicle->exitParkingLot();
            return true;
        }

        return false;
    }

    /**
     * Park or unpark the vehicle
     *
     * @param Vehicle $vehicle
     * @param ParkingLot $parkingLot
     * @param bool $park
     * @return bool
     * @throws \Exception
     */
    public function park(Vehicle $vehicle, ParkingLot $parkingLot, $park = true)
    {
        $pm = ParkingManager::getInstance();
        if($pm->vehicleIsInside($vehicle, $parkingLot)) {
            $parkingSpot = $pm->pickParkingSpot($parkingLot, $park);

            if(is_a($parkingSpot, ParkingSpot::class)) {
                if($parkingSpot->isOccupied() && $park) {
                    throw new \Exception("Sorry, this parking spot is taken.");
                }

                $parkingSpot->is_occupied = $park;
                $parkingSpot->save();
                $vehicle->parked($park);
            }
        }

        return $vehicle->is_parked;
    }

    /**
     * Creates vehicle in DB
     *
     * @return mixed
     */
    public function makeVehicle()
    {
        return Vehicle::create([]);
    }

    /**
     * Disallow cloning using 'clone'
     */
    private function __clone()
    {
    }

    /**
     * Disallow cloning using unserialize()
     */
    private function __wakeup()
    {
    }
}