Acest task pare a fi unul pentru un limbaj de programare cu suport multi-threading (ceea ce PHP nu este).
O alternativa ar fi un frontend care sa utilizeze AJAX (sau similar) pentru a monitoriza statisticile parcarii.
Din pacate, faptul ca PHP nu este multi-threaded nu ajuta la executarea in paralel a opeatiunilor propuse in fisierele input si output.
Totusi, am implementat cum am putut eu mai bine, in limita a 2 zile, solutia oferita.

Solutia este construita folosind Laravel ca framework, cu stocare MySQL.

Pentru a rula, din linia de comanda executati:
Mai intai introduceti cresentialele bazei de date in fisierul .env

Apoi, rulati migratiile
php artisan migrate (Doar prima data)

Rulati aplicatia
php artisan runinput:parking

Raspunsuri la intrebari:

In problema nefunctionarii sistemului, am propus un sistem prin care barierele pot fi setate pe "manual" astfel incat ele pot fi deschise manual.

In ceea ce priveste integrarea cu alte sisteme, ar trebui sa fie foarte simpla datorita claselor manager. In plus, sistemul este scalabil. Poate fi adaugat un numar nelimitat de parcari, locuri de parcare, bariere, etc.
